# Sh2Bash

Specify the full  path and recursivly change all #!/bin/sh shebang's,
to use bash. And quite a handy tool for Fish shell users.
As fish shell will error on files with the old POSIX Compliant #!/bin/sh
# How to use:

```bash
bash fix-shebang.sh
# (Enter PATH)
/path/to/Foo/Bar
# (Fixing...)
Fixed
```

# or, this way will skip the prompts for a path's

```bash
echo "/path/to/old-shebang-sh-file(s)" | bash fix-shebang.sh
```


